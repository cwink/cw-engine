#include "../../include/utilities/bits.hpp"

namespace cw {
namespace utils {
namespace bits {
auto buffer_from(const std::uint8_t *data, const size_t size) -> types::Buffer {
  if (data == nullptr) {
    throw std::runtime_error("Data can not be null.");
  }

  auto buffer = types::Buffer();

  for (auto ptr = data; static_cast<std::size_t>(ptr - data) < size; ++ptr) {
    buffer.emplace_back(*ptr);
  }

  return buffer;
}

auto data_from(const types::Buffer &buffer) noexcept -> std::pair<std::shared_ptr<std::uint8_t>, size_t> {
  auto data = std::shared_ptr<std::uint8_t>(new std::uint8_t[buffer.size()]);

  auto ptr = data.get();

  for (auto it = std::cbegin(buffer); it != std::cend(buffer); ++it, ++ptr) {
    *ptr = *it;
  }

  return std::make_pair(data, buffer.size());
}
} // namespace bits
} // namespace utils
} // namespace cw
