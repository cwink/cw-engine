#include "../../include/utilities/base64.hpp"
#include "../../include/utilities/bits.hpp"

namespace {
static constexpr std::int8_t encoding_table[]{
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
    'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

static constexpr std::uint8_t decoding_table[]{
    0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,  0,  0,  0,  62, 0,  0,  0,  63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61,
    0,  0,  0,  0,  0,  0,  0,  0, 1, 2, 3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24, 25, 0,  0,  0,  0, 0, 0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
    45, 46, 47, 48, 49, 50, 51, 0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0};
} // namespace

namespace cw {
namespace utils {
namespace base64 {
auto encode(const types::Buffer &buffer) noexcept -> std::string {
  auto string = std::string();

  for (auto it = std::cbegin(buffer); it < std::cend(buffer); it += 3) {
    switch (std::cend(buffer) - it) {
    case 1: {
      const auto bits{bits::mask<std::uint32_t>(16, 24) & (static_cast<std::uint32_t>(*it) << 16)};

      const auto index_a{(bits & bits::mask<std::uint32_t>(18, 24)) >> 18};

      const auto index_b{(bits & bits::mask<std::uint32_t>(12, 18)) >> 12};

      string.push_back(encoding_table[index_a]);

      string.push_back(encoding_table[index_b]);

      string.push_back('=');

      string.push_back('=');

      break;
    }
    case 2: {
      const auto bits_a{bits::mask<std::uint32_t>(16, 24) & (static_cast<std::uint32_t>(*it) << 16)};

      const auto bits_b{bits::mask<std::uint32_t>(8, 16) & (static_cast<std::uint32_t>(*(it + 1)) << 8)};

      const auto bits{bits_a | bits_b};

      const auto index_a{(bits & bits::mask<std::uint32_t>(18, 24)) >> 18};

      const auto index_b{(bits & bits::mask<std::uint32_t>(12, 18)) >> 12};

      const auto index_c{(bits & bits::mask<std::uint32_t>(6, 12)) >> 6};

      string.push_back(encoding_table[index_a]);

      string.push_back(encoding_table[index_b]);

      string.push_back(encoding_table[index_c]);

      string.push_back('=');

      break;
    }
    default: {
      const auto bits_a{bits::mask<std::uint32_t>(16, 24) & (static_cast<std::uint32_t>(*it) << 16)};

      const auto bits_b{bits::mask<std::uint32_t>(8, 16) & (static_cast<std::uint32_t>(*(it + 1)) << 8)};

      const auto bits_c{bits::mask<std::uint32_t>(0, 8) & static_cast<std::uint32_t>(*(it + 2))};

      const auto bits{bits_a | bits_b | bits_c};

      const auto index_a{(bits & bits::mask<std::uint32_t>(18, 24)) >> 18};

      const auto index_b{(bits & bits::mask<std::uint32_t>(12, 18)) >> 12};

      const auto index_c{(bits & bits::mask<std::uint32_t>(6, 12)) >> 6};

      const auto index_d{bits & bits::mask<std::uint32_t>(0, 6)};

      string.push_back(encoding_table[index_a]);

      string.push_back(encoding_table[index_b]);

      string.push_back(encoding_table[index_c]);

      string.push_back(encoding_table[index_d]);

      break;
    }
    }
  }

  return string;
}

auto decode(const std::string &string) noexcept -> types::Buffer {
  auto buffer = types::Buffer();

  for (auto it = std::cbegin(string); it < std::cend(string); it += 4) {
    const auto index_a{(static_cast<std::uint32_t>(decoding_table[static_cast<std::uint8_t>(*it)]) << 18) &
                       bits::mask<std::uint32_t>(18, 24)};

    const auto index_b{(static_cast<std::uint32_t>(decoding_table[static_cast<std::uint8_t>(*(it + 1))]) << 12) &
                       bits::mask<std::uint32_t>(12, 18)};

    const auto index_c{(static_cast<std::uint32_t>(decoding_table[static_cast<std::uint8_t>(*(it + 2))]) << 6) &
                       bits::mask<std::uint32_t>(6, 12)};

    const auto index_d{static_cast<std::uint32_t>(decoding_table[static_cast<std::uint8_t>(*(it + 3))]) &
                       bits::mask<std::uint32_t>(0, 6)};

    const auto bits{index_a | index_b | index_c | index_d};

    const auto bits_a{static_cast<std::uint8_t>(bits::mask<std::uint32_t>(0, 8) & (bits >> 16))};

    const auto bits_b{static_cast<std::uint8_t>(bits::mask<std::uint32_t>(0, 8) & (bits >> 8))};

    const auto bits_c{static_cast<std::uint8_t>(bits::mask<std::uint32_t>(0, 8) & bits)};

    if (*(it + 2) == '=' && *(it + 3) == '=') {
      buffer.emplace_back(bits_a);
    } else if (*(it + 3) == '=') {
      buffer.emplace_back(bits_a);

      buffer.emplace_back(bits_b);
    } else {
      buffer.emplace_back(bits_a);

      buffer.emplace_back(bits_b);

      buffer.emplace_back(bits_c);
    }
  }

  return buffer;
}
} // namespace base64
} // namespace utils
} // namespace cw
