#include "../../include/utilities/logging.hpp"

#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <vector>

namespace {
struct Call_stack {
  std::unordered_map<std::thread::id, std::vector<std::string>> stack;

  std::mutex mutex;
};

static auto was_stack_initialized = false;

static std::mutex stack_mutex;

static Call_stack *call_stack = nullptr;
} // namespace

namespace cw {
namespace utils {
namespace logging {
auto create_call_stack() noexcept -> void {
  stack_mutex.lock();

  call_stack = new Call_stack{};

  was_stack_initialized = true;

  stack_mutex.unlock();
}

auto destroy_call_stack() noexcept -> void {
  stack_mutex.lock();

  if (was_stack_initialized) {
    delete call_stack;
  }

  was_stack_initialized = false;

  stack_mutex.unlock();
}

auto push_to_call_stack(const std::string &file_name, const std::string &function_name, const std::size_t line_number)
    -> void {
  stack_mutex.lock();

  if (was_stack_initialized) {
    call_stack->mutex.lock();

    call_stack->stack[std::this_thread::get_id()].emplace_back(file_name + ":" + function_name + ":" +
                                                               std::to_string(line_number));

    call_stack->mutex.unlock();
  } else {
    stack_mutex.unlock();

    throw std::runtime_error("Call stack has not been created yet.");
  }

  stack_mutex.unlock();
}

auto pop_from_call_stack() -> void {
  stack_mutex.lock();

  if (was_stack_initialized) {
    call_stack->mutex.lock();

    call_stack->stack[std::this_thread::get_id()].pop_back();

    call_stack->mutex.unlock();
  } else {
    stack_mutex.unlock();

    throw std::runtime_error("Call stack has not been created yet.");
  }

  stack_mutex.unlock();
}

auto print_call_stack(std::ostream &stream, const size_t indent) -> void {
  stack_mutex.lock();

  if (was_stack_initialized) {
    call_stack->mutex.lock();

    for (auto it = --std::cend(call_stack->stack[std::this_thread::get_id()]);
         it >= std::cbegin(call_stack->stack[std::this_thread::get_id()]); --it) {
      stream << std::string(indent, ' ') << *it << std::endl;
    }

    call_stack->mutex.unlock();
  } else {
    stack_mutex.unlock();

    throw std::runtime_error("Call stack has not been created yet.");
  }

  stack_mutex.unlock();
}
} // namespace logging
} // namespace utils
} // namespace cw
