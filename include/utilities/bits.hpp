#ifndef CW_UTILITIES_BITS
#define CW_UTILITIES_BITS

#include "types.hpp"

#include <memory>
#include <stdexcept>

namespace cw {
namespace utils {
namespace bits {
/// \brief Creates a bitmask between the least significant bit and the most significant bit.
/// \tparam Type The type of the number to create the bitmask for.
/// \param lsb The least significant bit.
/// \param msb The most significant bit.
/// \return The bitmask in the form of the given type.
/// \throws std::runtime_error If the least significant bit is greater than or equal to the most significant bit or the
///                            most significant bit is greater than the bit size of the type.
template <typename Type, typename = std::enable_if_t<std::is_integral<Type>::value>>
inline auto mask(const size_t lsb, const size_t msb) -> Type {
  if (lsb >= msb) {
    throw std::runtime_error("Least significant bit is greater than or equal to the most significant bit.");
  }

  if (msb > (sizeof(Type) * 8)) {
    throw std::runtime_error("Most significant bit is greater than the bit size of the given type.");
  }

  const auto lsb_bits{(1 << lsb) - 1};

  const auto msb_bits{~((1 << msb) - 1)};

  return static_cast<Type>(~(lsb_bits | msb_bits));
}

/// \brief Converts a C style buffer data to a Buffer object.
/// \param data The C style buffer data.
/// \param size The size of the data.
/// \return A Buffer object created from the given data.
/// \throws std::runtime_error If the given data values is null.
auto buffer_from(const std::uint8_t *data, const size_t size) -> types::Buffer;

/// \brief Converts a Buffer object to a C style buffer.
/// \param buffer The buffer object to convert.
/// \return A pair containing a shared pointer to the data and the size of the data.
auto data_from(const types::Buffer &buffer) noexcept -> std::pair<std::shared_ptr<std::uint8_t>, size_t>;
} // namespace bits
} // namespace utils
} // namespace cw

#endif // CW_UTILITIES_BITS
