#ifndef CW_UTILITIES_RANDOM
#define CW_UTILITIES_RANDOM

#include "traits.hpp"

#include <random>

namespace cw {
namespace utils {
namespace random {
/// \brief Calculate a random number between the given minimum and maximum value.
/// \tparam Number The number type.
/// \param minimum The minimum value.
/// \param maximum The maximum value.
/// \return A randomly generated number between the given minimum and maximum value.
template <typename Number, typename = std::enable_if_t<std::is_arithmetic<Number>::value>>
inline auto number(const Number minimum, const Number maximum) noexcept -> Number {
  auto device = std::random_device();

  auto engine = std::mt19937(device());

  if constexpr (std::is_floating_point<Number>::value) {
    auto distribution = std::uniform_real_distribution<Number>(minimum, maximum);

    return distribution(engine);
  } else {
    auto distribution = std::uniform_int_distribution<Number>(minimum, maximum);

    return distribution(engine);
  }
}

/// \brief Retrive a random const reference to an item in a container between the minimum and maximum value locations.
/// \tparam Container The container type.
/// \param container The container.
/// \param minimum The minimum value location.
/// \param maximum The maximum value location.
/// \return A const reference to the randomly select item.
template <typename Container, typename = traits::evaluate<traits::is_const_iterable<Container>::value &&
                                                          traits::is_sizable<Container>::value &&
                                                          traits::has_underlying_type<Container>::value>>
inline auto item(const Container &container, const size_t minimum, const size_t maximum) -> const
    typename Container::value_type & {
  if (maximum >= container.size()) {
    throw std::runtime_error("Maximum values is greater than containers size.");
  }

  return *(std::next(container.cbegin(), static_cast<typename Container::difference_type>(number(minimum, maximum))));
}

/// \brief Retrive a random reference to an item in a container between the minimum and maximum value locations.
/// \tparam Container The container type.
/// \param container The container.
/// \param minimum The minimum value location.
/// \param maximum The maximum value location.
/// \return A reference to the randomly select item.
/// \throws std::runtime_error If the maximum value is greater than the container size.
template <typename Container, typename = traits::evaluate<traits::is_const_iterable<Container>::value &&
                                                          traits::is_sizable<Container>::value &&
                                                          traits::has_underlying_type<Container>::value>>
inline auto item(Container &container, const size_t minimum, const size_t maximum) -> typename Container::value_type & {
  if (maximum >= container.size()) {
    throw std::runtime_error("Maximum values is greater than containers size.");
  }

  return *(std::next(container.cbegin(), static_cast<typename Container::difference_type>(number(minimum, maximum))));
}
} // namespace random
} // namespace utils
} // namespace cw

#endif // CW_UTILITIES_RANDOM
