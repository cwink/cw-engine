#ifndef CW_UTILITIES_COMPARATORS
#define CW_UTILITIES_COMPARATORS

namespace cw {
namespace utils {
namespace comp {
template <typename Type, typename = std::enable_if_t<std::is_arithmetic<Type>::value>>
inline auto equal(const Type &left, const Type &right) noexcept -> bool {
  if constexpr (std::is_floating_point<Type>::value) {
    return std::fabs(static_cast<double>(left) - static_cast<double>(right)) < 0.00001;
  } else {
    return left == right;
  }
}
} // namespace comp
} // namespace utils
} // namespace cw

#endif // CW_UTILITIES_COMPARATORS
