#ifndef CW_UTILITIES_BASE64
#define CW_UTILITIES_BASE64

#include "types.hpp"

#include <string>

namespace cw {
namespace utils {
namespace base64 {
/// \brief Takes a buffer of bytes and converts it to a base64 string.
/// \param buffer The buffer to convert.
/// \return The base64 encoded string.
auto encode(const types::Buffer &buffer) noexcept -> std::string;

/// \brief Takes a base64 string and converts it to a buffer of bytes.
/// \param string The base64 string to convert.
/// \return The buffered bytes of the string.
auto decode(const std::string &string) noexcept -> types::Buffer;
} // namespace base64
} // namespace utils
} // namespace cw

#endif // CW_UTILITIES_BASE64
