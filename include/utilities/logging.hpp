#include <iostream>
#include <string>

/// \brief Push the current function onto the callstack.
#define CW_UTILS_LOGGING_CALL_STACK_PUSH() cw::utils::logging::push_to_call_stack(__FILE__, __func__, __LINE__)

/// \brief Pop the current function off the callstack.
#define CW_UTILS_LOGGING_CALL_STACK_POP() cw::utils::logging::pop_from_call_stack()

/// \brief Print the callstack to stderr with a default indent of 2 spaces.
#define CW_UTILS_LOGGING_CALL_STACK_PRINT() cw::utils::logging::print_call_stack(std::cerr, 2)

namespace cw {
namespace utils {
namespace logging {
/// \brief Creates the callstack.
auto create_call_stack() noexcept -> void;

/// \brief Destroy's the callstack.
auto destroy_call_stack() noexcept -> void;

/// \brief Pushes the given function onto the callstack.
/// \param file_name The filename to use (typically __FILE__).
/// \param function_name The function name to use (typically __func__).
/// \param line_number The line number to use (typically __LINE__).
/// \throws std::runtime_error If the callstack has not been created yet.
auto push_to_call_stack(const std::string &file_name, const std::string &function_name, const std::size_t line_number)
    -> void;

/// \brief Pop the current function from the callstack.
/// \throws std::runtime_error If the callstack has not been created yet.
auto pop_from_call_stack() -> void;

/// \brief Print the callstack to the given string with the number of spaces to indent.
/// \param stream The output stream to print to.
/// \param indent The indentation to use.
/// \throws std::runtime_error If the callstack has not been created yet.
auto print_call_stack(std::ostream &stream, const size_t indent) -> void;
} // namespace logging
} // namespace utils
} // namespace cw
