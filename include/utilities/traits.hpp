#ifndef CW_UTILITIES_TRAITS
#define CW_UTILITIES_TRAITS

#include <type_traits>

namespace cw {
namespace utils {
namespace traits {
/// \brief Evaluate a condition, returning either a true or false type.
/// \tparam Condition The condition to evaluate.
template <const bool Condition>
using evaluate = std::conditional_t<(Condition) == true, std::true_type, std::false_type>;

template <typename, typename = void> struct is_iterable : std::false_type {};

/// \brief Check if a type is iterable.
/// \tparam Type the type to check.
template <typename Type>
struct is_iterable<Type, std::void_t<decltype(std::declval<Type>().begin()), decltype(std::declval<Type>().end()),
                                     typename Type::difference_type, typename Type::iterator>> : std::true_type {};

template <typename, typename = void> struct is_const_iterable : std::false_type {};

/// \brief Check if a type is const iterable.
/// \tparam Type the type to check.
template <typename Type>
struct is_const_iterable<Type,
                         std::void_t<decltype(std::declval<Type>().cbegin()), decltype(std::declval<Type>().cend()),
                                     typename Type::difference_type, typename Type::const_iterator>> : std::true_type {
};

template <typename, typename = void> struct is_sizable : std::false_type {};

/// \brief Check if a type is sizable.
/// \tparam Type the type to check.
template <typename Type>
struct is_sizable<Type, std::void_t<decltype(std::declval<Type>().size()), decltype(std::declval<Type>().empty()),
                                    decltype(std::declval<Type>().reserve(2)), decltype(std::declval<Type>().capacity()),
                                    decltype(std::declval<Type>().shrink_to_fit()), typename Type::size_type>>
    : std::true_type {};

template <typename, typename = void> struct has_underlying_type : std::false_type {};

/// \brief Check if a type has an underlying type.
/// \tparam Type the type to check.
template <typename Type> struct has_underlying_type<Type, std::void_t<typename Type::value_type>> : std::true_type {};
} // namespace traits
} // namespace utils
} // namespace cw

#endif // CW_UTILITIES_TRAITS
