#ifndef CW_UTILITIES_TYPES
#define CW_UTILITIES_TYPES

#include <vector>
#include <cstdint>

namespace cw {
namespace utils {
namespace types {
/// \brief Represents a data buffer in memory.
using Buffer = std::vector<std::uint8_t>;
} // namespace types
} // namespace utils
} // namespace cw

#endif // CW_UTILITIES_TYPES
