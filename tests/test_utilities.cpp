#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "../include/utilities/base64.hpp"
#include "../include/utilities/bits.hpp"
#include "../include/utilities/comparators.hpp"
#include "../include/utilities/logging.hpp"
#include "../include/utilities/random.hpp"
#include "../include/utilities/traits.hpp"

#include <list>
#include <stdexcept>
#include <thread>
#include <utility>
#include <vector>

SCENARIO("When testing traits, the results are correct.") {
  GIVEN("Nothing.") {
    WHEN("Traits are tested.") {
      THEN("The results are correct.") {
        REQUIRE(std::is_same<cw::utils::traits::evaluate<1 == 1>, std::true_type>::value);

        REQUIRE(std::is_same<cw::utils::traits::evaluate<1 == 0>, std::false_type>::value);

        REQUIRE(cw::utils::traits::is_iterable<std::vector<int>>::value);

        REQUIRE(!cw::utils::traits::is_iterable<std::pair<int, int>>::value);

        REQUIRE(cw::utils::traits::is_const_iterable<std::vector<int>>::value);

        REQUIRE(!cw::utils::traits::is_const_iterable<std::pair<int, int>>::value);

        REQUIRE(cw::utils::traits::is_sizable<std::vector<int>>::value);

        REQUIRE(!cw::utils::traits::is_sizable<std::pair<int, int>>::value);

        REQUIRE(cw::utils::traits::has_underlying_type<std::vector<int>>::value);

        REQUIRE(!cw::utils::traits::has_underlying_type<std::pair<int, int>>::value);
      }
    }
  }
}

SCENARIO("When testing random, the results are correct.") {
  GIVEN("A few random numbers and a few container.") {
    const auto number_a = cw::utils::random::number(3, 8);

    const auto number_b = cw::utils::random::number(1.2f, 9.3f);

    const auto container_a = std::vector<int>({1, 2, 3, 4, 5});

    const auto container_b = std::list<int>({1, 2, 3, 4, 5});

    WHEN("Random functionality is tested.") {
      THEN("The results are correct.") {
        REQUIRE((number_a >= 3 && number_a <= 8));

        REQUIRE(std::is_integral<decltype(number_a)>::value);

        REQUIRE((number_b >= 1.2f && number_b <= 9.3f));

        REQUIRE(std::is_floating_point<decltype(number_b)>::value);

        REQUIRE(cw::utils::random::item(container_a, 2, 4));

        REQUIRE_THROWS(cw::utils::random::item(container_a, 2, 5));

        REQUIRE(cw::utils::random::item(container_b, 2, 4));

        REQUIRE_THROWS(cw::utils::random::item(container_b, 2, 5));
      }
    }
  }
}

SCENARIO("When testing bits, the results are correct.") {
  GIVEN("A few values.") {
    const auto troll_a = cw::utils::types::Buffer({'T', 'r', 'o', 'l', 'l', 'o', 'l'});

    const std::uint8_t troll_b[7]{'T', 'r', 'o', 'l', 'l', 'o', 'l'};

    WHEN("Bit functionality is tested.") {
      THEN("The results are correct.") {
        REQUIRE(cw::utils::bits::mask<std::uint8_t>(2, 4) == 12);

        REQUIRE(cw::utils::bits::mask<std::uint32_t>(16, 23) == 8323072);

        REQUIRE_THROWS(cw::utils::bits::mask<std::uint8_t>(4, 2));

        REQUIRE_THROWS(cw::utils::bits::mask<std::uint8_t>(2, 9));

        REQUIRE(cw::utils::bits::buffer_from(troll_b, 7) == troll_a);

        REQUIRE_THROWS(cw::utils::bits::buffer_from(nullptr, 7));

        const auto data = cw::utils::bits::data_from(troll_a);

        REQUIRE(data.second == 7);

        for (auto i = static_cast<std::size_t>(0); i < data.second; ++i) {
          REQUIRE(data.first.get()[i] == troll_b[i]);
        }
      }
    }
  }
}

SCENARIO("When testing base64, the results are correct.") {
  GIVEN("A few values.") {
    const auto moon =
        cw::utils::types::Buffer({'T', 'h', 'e', ' ', 'm', '0', '0', 'n', ' ', 'i', 's', ' ', 'a', ' ', '+',
                                  'h', 'a', 'r', 's', 'h', ' ', 'M', 'i', '$', 't', 'r', 'e', 's', 's', '.'});

    const auto troll_a = cw::utils::types::Buffer({'T', 'r', 'o', 'l', 'l', 'o', 'l'});

    const auto troll_b = cw::utils::types::Buffer({'T', 'r', 'o', 'l', 'l'});

    WHEN("Base64 functionality is tested.") {
      THEN("The results are correct.") {
        REQUIRE(cw::utils::base64::encode(moon) == "VGhlIG0wMG4gaXMgYSAraGFyc2ggTWkkdHJlc3Mu");

        REQUIRE(cw::utils::base64::encode(troll_a) == "VHJvbGxvbA==");

        REQUIRE(cw::utils::base64::encode(troll_b) == "VHJvbGw=");

        REQUIRE(cw::utils::base64::decode("VGhlIG0wMG4gaXMgYSAraGFyc2ggTWkkdHJlc3Mu") == moon);

        REQUIRE(cw::utils::base64::decode("VHJvbGxvbA==") == troll_a);

        REQUIRE(cw::utils::base64::decode("VHJvbGw=") == troll_b);
      }
    }
  }
}

SCENARIO("When testing comparators, the results are correct.") {
  GIVEN("Nothing.") {
    WHEN("Comparator functionality is tested.") {
      THEN("The results are correct.") {
        REQUIRE(cw::utils::comp::equal(1.23f, 1.23f));

        REQUIRE(!cw::utils::comp::equal(1.22f, 1.23f));

        REQUIRE(cw::utils::comp::equal(1.23, 1.23));

        REQUIRE(!cw::utils::comp::equal(1.22, 1.23));

        REQUIRE(cw::utils::comp::equal(1, 1));

        REQUIRE(!cw::utils::comp::equal(1, 2));
      }
    }
  }
}

[[noreturn]] auto function1() -> void;

[[noreturn]] auto function2() -> void;

[[noreturn]] auto function3() -> void;

[[noreturn]] auto function4() -> void;

[[noreturn]] auto function5() -> void;

[[noreturn]] auto function6() -> void;

[[noreturn]] auto function7() -> void;

[[noreturn]] auto function8() -> void;

[[noreturn]] auto function9() -> void;

auto function9() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 9);

  throw std::runtime_error("Another damn error occured.");

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function8() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 8);

  function9();

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function7() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 7);

  function8();

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function6() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 6);

  throw std::runtime_error("Another error occured.");

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function5() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 5);

  function6();

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function4() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 4);

  function5();

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function3() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 3);

  throw std::runtime_error("An error occured.");

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function2() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 2);

  function3();

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

auto function1() -> void {
  cw::utils::logging::push_to_call_stack(__FILE__, __func__, 1);

  function2();

  CW_UTILS_LOGGING_CALL_STACK_POP();
}

SCENARIO("When testing logging functionality, the results are correct.") {
  GIVEN("Nothing.") {
    WHEN("Logging functionality is tested.") {
      THEN("The results are correct.") {
        REQUIRE_THROWS(CW_UTILS_LOGGING_CALL_STACK_PUSH());

        REQUIRE_THROWS(CW_UTILS_LOGGING_CALL_STACK_POP());

        REQUIRE_THROWS(CW_UTILS_LOGGING_CALL_STACK_PRINT());

        cw::utils::logging::create_call_stack();

        auto stream1 = std::ostringstream();

        auto stream2 = std::ostringstream();

        auto stream3 = std::ostringstream();

        try {
          function1();
        } catch (std::exception &exception) {
          stream1 << exception.what() << std::endl;

          cw::utils::logging::print_call_stack(stream1, 2);
        }

        auto thread1 = std::thread([&]() {
          try {
            function4();
          } catch (std::exception &exception) {
            stream2 << exception.what() << std::endl;

            cw::utils::logging::print_call_stack(stream2, 2);
          }
        });

        auto thread2 = std::thread([&]() {
          try {
            function7();
          } catch (std::exception &exception) {
            stream3 << exception.what() << std::endl;

            cw::utils::logging::print_call_stack(stream3, 2);
          }
        });

        thread1.join();

        thread2.join();

        REQUIRE(stream1.str() == std::string("An error occured.\n  ") + __FILE__ + ":function3:3\n  " __FILE__ +
                                     ":function2:2\n  " + __FILE__ + ":function1:1\n");

        REQUIRE(stream2.str() == std::string("Another error occured.\n  ") + __FILE__ + ":function6:6\n  " __FILE__ +
                                     ":function5:5\n  " + __FILE__ + ":function4:4\n");

        REQUIRE(stream3.str() == std::string("Another damn error occured.\n  ") + __FILE__ +
                                     ":function9:9\n  " __FILE__ + ":function8:8\n  " + __FILE__ + ":function7:7\n");

        cw::utils::logging::destroy_call_stack();
      }
    }
  }
}
